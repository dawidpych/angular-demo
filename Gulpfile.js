"use strict";

var del = require("del"),
    gulp = require("gulp"),
    karma = require('karma').Server,
    less = require("gulp-less"),
    maps = require("gulp-sourcemaps"),
    htmlreplace = require("gulp-html-replace"),
    sync = require("browser-sync").create(),
    modrewrite = require("connect-modrewrite"),
    runSequence = require("run-sequence"),
    rjs = require("requirejs");


gulp.task("browserSync", function () {
    sync.init({
        server: {
            baseDir: "src",
            middleware: [
                modrewrite(["^([^.]+)$ /index.html [L]"])
            ]
        }
    });
});

gulp.task("css", function () {
    gulp.src("src/less/style.less")
        .pipe(maps.init())
        .pipe(less())
        .pipe(maps.write("."))
        .pipe(gulp.dest("src/assets/css/"));
});

gulp.task("build:copy", function () {
    //copy data.
    gulp.src("src/data/**/*.json")
        .pipe(gulp.dest("dist/data"));

    //copy styles for bootstrap, fa etc.
    gulp.src([
            "src/bower_components/bootstrap/dist/css/bootstrap.min.css",
            "src/bower_components/font-awesome/css/font-awesome.min.css"
        ])
        .pipe(gulp.dest("dist/assets/css/"));

    //copy fonts for bootstrap, fa etc.
    gulp.src([
            "src/bower_components/bootstrap/dist/fonts/*",
            "src/bower_components/font-awesome/fonts/*"
        ])
        .pipe(gulp.dest("dist/assets/fonts/"));

    //copy html templates
    gulp.src("src/app/views/**/*.html")
        .pipe(gulp.dest("dist/app/views/"));

    //copy index filter
    gulp.src("src/index.html")
        .pipe(htmlreplace({
            css: [
                "/assets/css/bootstrap.min.css",
                "/assets/css/font-awesome.min.css"
	    ],
            js: "/app/js/build.js"
	}))
        .pipe(gulp.dest("dist"));
});

gulp.task("build:css", function () {
    gulp.src("src/less/style.less")
        .pipe(maps.init())
        .pipe(less())
        .pipe(maps.write("."))
        .pipe(gulp.dest("dist/assets/css/"));
});

gulp.task("build:app", function () {
    var config = {
        baseUrl: "src/app",
        name: "init",
        optimize: "uglify2",
        include: ["../bower_components/requirejs/require.js"],
        paths: {
            "angular": "../bower_components/angular/angular.min",
            "ngRoute": "../bower_components/angular-route/angular-route.min"
        },
        shim:{
            "angular": {
                exports: "angular"
            },
            "ngRoute": {
                deps: ["angular"]
            }
        },
        out: "dist/app/js/build.js"
    };

    rjs.optimize(config);
});

gulp.task("build:clear", function () {
    del.sync("dist");
});

gulp.task("test:unit", function (done) {
    new karma({
        configFile: __dirname + "/karma.conf.js",
        singleRun: true
    }, done).start();
});

gulp.task("build", function () {
    runSequence("test:unit", "build:clear", "build:app", "build:css", "build:copy");
});

gulp.task("watch", ["browserSync"], function () {
    gulp.watch("src/less/**/*.less", ["css"]);
    gulp.watch("src/*.html", sync.reload);
    gulp.watch("src/app/**/*", sync.reload);
    gulp.watch("src/assets/js/**/*.js", sync.reload);
    gulp.watch("src/assets/css/**/*.css", sync.reload);
});
