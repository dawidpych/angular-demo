"use strict";

requirejs.config({
   baseUrl: "app",

   nodeRequire: [require],

   paths: {
       "jquery": "../bower_components/jquery/dist/jquery.min",
       "angular": "../bower_components/angular/angular.min",
       "ngRoute": "../bower_components/angular-route/angular-route.min"
   },

   shim:{
       "jquery": {
           export: "jquery",
           validate: function(){
               return  window.jQuery.Defferred;
           }
       },
       "angular": {
           exports: "angular",
           deps: ["jquery"]
       },
       "ngRoute": {
           deps: ["angular"]
       }
   }
});

requirejs([
    "jquery",
    "angular",
    "ngRoute",
    "app"
], function ($, angular) {
    $(function() {
        angular.bootstrap(document, ["App"]);
    });
});
