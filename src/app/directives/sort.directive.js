"use strict";

define(function (require) {
    var angular = require("angular");

    /**
     * Directive to display sort icon.
     * Require:
     * 	- "direction" {ngModel | Boolean} property to display direction.
     *
     * To use just add element.
     * Example:
     *
     * <xf-sort direction="direction">Any name</xf-sort>
     */
    function AppSortDirective () {
        var directive = {
            restrict: "E",
            link: link,
            templateUrl: "app/views/sort.directive.view.html",
            scope: {
                direction: "="
            },
            transclude: true
        };

        return directive;

        function link (scope) {
            scope.isDirection = false;
            scope.changeDirection = changeDirection;

            scope.$watch("direction", function () {
                scope.isDirection = angular.isDefined(scope.direction);
            })

            /**
             * Force display direction icon after click.
             */
            function changeDirection () {
                scope.isDirection = true;
            }
        }
    }

    AppSortDirective.$inject = [];

    return AppSortDirective;
});
