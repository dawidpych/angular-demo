"use strict";

define(function () {

    /**
     * Directive used to display header with menu.
     *
     * To use directive you must just create element <xf-header></xf-header>
     *
     * @param {Object}  $location    locations service
     * @param {Array}   AppMenu       menu data.
     */
    function AppHeaderDirective ($location, AppMenu) {
        var directive = {
            restrict: "E",
            link: link,
            replace: true,
            templateUrl: "app/views/header.directive.view.html",
            scope: {}
        };

        return directive;

        function link (scope) {
            scope.current = current;
            scope.menus = AppMenu;
            scope.collapseIn = false;
            scope.collapse = collapse;

            /**
             * Get current url.
             */
            function current () {
                return $location.url();
            }

            /**
             * Toggel mobile menu.
             * @param  {Boolean} show To force show or hide mobile menu.
             */
            function collapse (show) {
                scope.collapseIn = show || !scope.collapseIn;
            }
        }
    }

    AppHeaderDirective.$inject = ["$location", "AppMenu"];

    return AppHeaderDirective;
});
