"use strict";

define(function () {
    /**
     * Default main app controller.
     */
    function AppController () {}

    AppController.$inject = [];

    return AppController;
});
