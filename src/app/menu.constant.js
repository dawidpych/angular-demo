"use strict";

define(function () {
    /**
     * Menu and routing config.
     */
    return [
        {
            name: "Contributors",
            url: "/contributors",
            details: {
                controller: "ContributorsController",
                controllerAs: "cc",
                templateUrl: "/app/views/contributors.view.html"
            }
        },
        {
            name: "Repositories",
            url: "/repositories",
            details: {
                controller: "RepositoriesController",
                controllerAs: "rc",
                templateUrl: "/app/views/repositories.view.html"
            }
        }
    ]
});
