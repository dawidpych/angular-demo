"use strict";

define(function (require) {
    var angular = require("angular");

    /**
     * App config function.
     * @param {Object} $routeProvider     Routing provider
     * @param {Array} AppMenu             Menu and routing setup
     * @param {Object} GitServiceProvider Git service.
     */
    function AppConfig ($routeProvider, AppMenu, GitServiceProvider) {
        GitServiceProvider.setOrganisation("x-formation");

        angular.forEach(AppMenu, function (menu) {
            $routeProvider.when(menu.url, menu.details);
        });

        $routeProvider.otherwise("/contributors");
    }

    AppConfig.$inject = ["$routeProvider", "AppMenu", "GitServiceProvider"];

    return AppConfig;
});
