"use strict";

requirejs([
    "angular",
    "ngRoute",
    "app"
], function (angular) {
    angular.bootstrap(document, ["App"]);
});
