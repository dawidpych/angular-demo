"use strict";

define(function () {
    function RepositoriesController (GitService) {
        var self = this;

        /**
         * Data from git. List of repositories.
         * @type {Array}
         */
        self.repositories = [];

        /**
         * Direction of order.
         * @type {Boolean}
         */
        self.revers = true;

        /**
         * Column to sort
         * @type {String}
         */
        self.column = "forks";

        /**
         * Setting for sort directive.
         * @type {Object}
         */
        self.columns = {
            forks: true
        };

        /**
         * To show or hide loading animation.
         * @type {Boolean}
         */
        self.loadingData = true;

        self.sortBy = sortBy;

        init();

        /**
         * Init methos to get data.
         */
        function init() {
            GitService.getOrgRepos().then(function (response) {
                self.repositories = response.data;
            }).finally(function () {
                self.loadingData = false;
            });
        }

        /**
         * Method to sort column.
         * @param  {String} name name of data used to sort.
         */
        function sortBy (name) {
            if (angular.isDefined(self.columns[name])) {
                self.columns[name] = !self.columns[name];
            } else {
                self.columns = {};
                self.columns[name] = true;
            }

            self.column = name;
            self.revers = self.columns[name];
        }
    }

    RepositoriesController.$inject = ["GitService"];

    return RepositoriesController;
});
