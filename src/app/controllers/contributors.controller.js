"use strict";

define(function (require) {
    var angular = require("angular");

    function ContributorsController (AppContributorsService) {
        var self = this;

         /**
          * Direction of order.
          * @type {Boolean}
          */
        self.revers = true;

        /**
         * Column to sort
         * @type {String}
         */
        self.column = "contributions";

        /**
         * Setting for sort directive.
         * @type {Object}
         */
        self.columns = {
            contributions: true
        };

        self.contributors = AppContributorsService.items;
        self.sortBy = sortBy;


        /**
         * Method to sort column.
         * @param  {String} name name of data used to sort.
         */
        function sortBy (name) {
            if (angular.isDefined(self.columns[name])) {
                self.columns[name] = !self.columns[name];
            } else {
                self.columns = {};
                self.columns[name] = true;
            }

            self.column = name;
            self.revers = self.columns[name];
        }
    }

    ContributorsController.$inject = ["AppContributorsService"];

    return ContributorsController;
});
