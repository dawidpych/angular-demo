"use strict";

define(function () {

    /**
     * Service to git access/
     */
    function GitServiceProvider () {
        var options = {
                organisation: null,
                host: "http://api.github.com/"
            },
            provider = {
                $get: get,
                setOrganisation: setOrganisation,
                setHost: setHost
            };

        get.$inject = ["$http"];

        return provider;

        /**
         * Main service.
         * @param  {[type]} $http http service
         * @return {Object}       return service
         */
        function get ($http) {
            var service = {
                getOrgRepos: getOrgRepos
            };

            return service;

            /**
             * Get organisation repositories from git.
             * 
             * @param {String} organisationName  name of organisation in github for requst.
             *                                   If not set it will be get defaul organisation name.
             * @return {Promise}                 Promise for request.
             */
            function getOrgRepos (organisationName) {
                var url = options.host + "orgs/" + (options.organisation || organisationName) + "/repos";
                return $http.get(url);
            }
        }

        /**
         * Set default organisation name.
         * @param {String} name name of organisation in github.
         */
        function setOrganisation (name) {
            options.organisation = name;
        }

        /**
         * Set git host url.
         * @param {String} hostname hostname of git service. Example https://api.github.com/
         */
        function setHost (hostname) {
            options.host = hostname;
        }
    }

    GitServiceProvider.$inject = [];

    return GitServiceProvider;
});
