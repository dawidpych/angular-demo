"use strict";

define(function (require) {
    var angular = require("angular");

    /**
     * Service for contributors.
     * @param {Object} $http http service/
     */
    function AppContributorsFactory ($http) {
        var self = this;

        self.items = [];
        self.load = load;

        load();

        /**
         * Load contributors list from local data.
         * @param  {Function} callback callback when load is succesfuly finished.
         */
        function load(callback) {
            $http
                .get("/data/contributors.json")
                .then(function (response) {
                    angular.copy(response.data, self.items);

                    if (angular.isFunction(callback)) {
                        callback(self.items);
                    }
            });
        }
    }

    AppContributorsFactory.$inject = ["$http"];

    return AppContributorsFactory;
});
