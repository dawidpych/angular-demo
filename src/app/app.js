"use strict";

define(function (require) {
    var angular = require("angular"),
        AppConfig = require("app.config"),
        AppController = require("app.controller"),
        AppContributorsService = require("services/contributors.services"),
        AppHeaderDirective = require("directives/header.directive"),
        AppMenu = require("menu.constant"),
        AppSortDirective = require("directives/sort.directive"),
        ContributorsController = require("controllers/contributors.controller"),
        RepositoriesController = require("controllers/repositories.controller"),
        GitServiceProvider = require("providers/git.provider");

    angular
        .module("App", ["ngRoute"])
        .config(AppConfig)
        .constant("AppMenu", AppMenu)
        .controller("AppController", AppController)
        .controller("ContributorsController", ContributorsController)
        .controller("RepositoriesController", RepositoriesController)
        .directive("xfHeader", AppHeaderDirective)
        .directive("xfSort", AppSortDirective)
        .provider("GitService", GitServiceProvider)
        .service("AppContributorsService", AppContributorsService);
});
