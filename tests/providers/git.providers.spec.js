"use strict";

define(function (require) {
    describe("git.providers.js ", function () {
        var provider, GitServiceProvider = require("providers/git.provider");

        beforeEach(function () {
            provider = GitServiceProvider();
        });

        it("should be defined", function () {
            var $http = {
                get: function(url) {
                    return url;
                }
            };

            spyOn($http, "get");

            provider.setHost("hostname/");

            provider.$get($http).getOrgRepos();

            expect($http.get).toHaveBeenCalledWith("hostname/orgs/undefined/repos");
        });
    });
});
