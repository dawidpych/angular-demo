"use strict";

define(function (require) {
    var AppController = require("app.controller");

    describe("app.controller.js ", function () {
        it("should be defined", function () {
            var controller = new AppController();

            expect(controller).toBeDefined();
        });
    });
});
