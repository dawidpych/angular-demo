"use strict";

var tests = [];
for (var file in window.__karma__.files) {
  if (window.__karma__.files.hasOwnProperty(file)) {
    if (/spec\.js$/.test(file)) {
      tests.push(file);
    }
  }
}

requirejs.config({
    baseUrl: "/base/src/app",
    nodeRequire: [require],

    paths: {
        "angular": "../bower_components/angular/angular.min",
        "angular-mocks": "../../node_modules/angular-mocks/angular-mocks",
        "ngRoute": "../bower_components/angular-route/angular-route.min"
    },

    shim:{
        "angular": {
            exports: "angular",
        },
        "angular-mocks": {
            deps: ["angular"]
        },
        "ngRoute": {
            deps: ["angular"]
        }
    },
    deps: tests,
    callback: window.__karma__.start
 });
