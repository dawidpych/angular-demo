"use strict";

define(function (require) {
    var ContributorsController = require("controllers/contributors.controller");

    describe("contributors.controller.js ", function () {
        var controller,
            AppContributorsService = {};

        beforeEach(function () {
            controller = new ContributorsController(AppContributorsService);
        });

        it("should be defined", function () {
            expect(controller).toBeDefined();
        });

        it("should be sorted by name", function () {
            var column = "name";
            controller.sortBy(column);

            expect(controller.column).toEqual(column);
            expect(controller.revers).toEqual(true);
            expect(controller.columns).toEqual({"name": true});
        });

        it("should be asc sort on name column after secound click", function () {
            var column = "name";
            controller.sortBy(column);
            controller.sortBy(column);

            expect(controller.column).toEqual(column);
            expect(controller.revers).toEqual(false);
            expect(controller.columns).toEqual({"name": false});
        });
    });
});
