"use strict";

define(function (require) {

    require("angular");
    require("angular-mocks");
    require("ngRoute");
    require("app");

    describe("repositories.controller.js ", function () {
        var $httpBackend, controller,
            respond = [
                {name: "repo1"},
                {name: "repo2"}
            ];

        beforeEach(module("App"));
        beforeEach(inject(function (_$httpBackend_, $controller) {
             $httpBackend = _$httpBackend_;
             controller = $controller("RepositoriesController", {});

             $httpBackend
                .when("GET", "http://api.github.com/orgs/x-formation/repos")
                .respond(respond);
        }));

        it("should be defined", function () {
            $httpBackend.expectGET("http://api.github.com/orgs/x-formation/repos");
            $httpBackend.flush();
            expect(controller.repositories).toEqual(respond);
            expect(controller).toBeDefined();
        });

        it("should show loading when http filed", function () {
            $httpBackend.expectGET("http://api.github.com/orgs/x-formation/repos").respond(404);
            $httpBackend.flush();

            expect(controller.loadingData).toEqual(false);
        });

        it("should be sorted by name", function () {
            var column = "name";
            controller.sortBy(column);

            expect(controller.column).toEqual(column);
            expect(controller.revers).toEqual(true);
            expect(controller.columns).toEqual({"name": true});
        });

        it("should be asc sort on name column after secound click", function () {
            var column = "name";
            controller.sortBy(column);
            controller.sortBy(column);

            expect(controller.column).toEqual(column);
            expect(controller.revers).toEqual(false);
            expect(controller.columns).toEqual({"name": false});
        });
    });
});
