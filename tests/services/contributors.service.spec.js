"use strict";

define(function (require) {

    require("angular");
    require("angular-mocks");
    require("ngRoute");
    require("app");

    describe("contributors.service.js ", function () {
        var $httpBackend, service,
            respond = [
                {name: "item1"},
                {name: "item2"}
            ];

        beforeEach(module("App"));
        beforeEach(inject(function (_$httpBackend_, AppContributorsService) {
             $httpBackend = _$httpBackend_;
             service = AppContributorsService;

             $httpBackend
                .when("GET", "/data/contributors.json")
                .respond(respond);
        }));

        it("should be defined", function () {
            $httpBackend.expectGET("/data/contributors.json");
            $httpBackend.flush();

            expect(service.items).toEqual(respond);
            expect(service).toBeDefined();
        });

        it("should be empty array when download filed", function () {
            $httpBackend
                .expectGET("/data/contributors.json")
                .respond(404);
            $httpBackend.flush();

            expect(service.items).toEqual([]);
        });

        it("should load data when call method", function () {
            var newRespond = [{}, {}];

            $httpBackend
               .expectGET("/data/contributors.json")
               .respond([{}, {}]);

            $httpBackend.flush();

            service.load();

            expect(service.items).toEqual(newRespond);
        });

        it("should callback call method", function () {
            var callback = {
                fn: function() {
                    return true;
                }
            };

            spyOn(callback, "fn");

            $httpBackend
               .expectGET("/data/contributors.json");

            service.load(callback.fn);
            $httpBackend.flush();

            expect(callback.fn).toHaveBeenCalled();
        });

        it("shouldn`t callback call method", function () {
            var callbackTest = false;

            $httpBackend
                .expectGET("/data/contributors.json")
                .respond(404);
            $httpBackend.flush();

            service.load(function () {
                callbackTest = true;
                expect(callbackTest).toEqual(false);
                return;
            });
            expect(callbackTest).toEqual(false);
        });
    });
});
