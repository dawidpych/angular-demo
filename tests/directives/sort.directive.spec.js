"use strict";

define(function (require) {

    require("angular");
    require("angular-mocks");
    require("ngRoute");
    require("app");

    describe("repositories.controller.js ", function () {
        var $compile, $rootSscope, scope;

        beforeEach(module("App"));
        beforeEach(inject(function (_$compile_, _$rootScope_, $templateCache) {
             $compile = _$compile_;
             $rootSscope = _$rootScope_;
             scope = $rootSscope.$new();

             $templateCache.put("app/views/sort.directive.view.html","<div>Fake</div>");
        }));

        it("Should set is direction", function () {
            var element = $compile("<xf-sort>Any name</xf-sort>")(scope);

            scope.$digest();

            var isolateScope = element.isolateScope();

            expect(isolateScope.isDirection).toBeFalsy();

            isolateScope.changeDirection();

            expect(isolateScope.isDirection).toBeTruthy();
        });
    });
});
