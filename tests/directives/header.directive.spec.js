"use strict";

define(function (require) {

    require("angular");
    require("angular-mocks");
    require("ngRoute");
    require("app");

    describe("repositories.controller.js ", function () {
        var $compile, $rootSscope, scope;

        beforeEach(module("App"));
        beforeEach(module(function($provide) {
            $provide.value("$location", {
                url: function () {
                    return "testUrl";
                }
            });
        }));
        beforeEach(inject(function (_$compile_, _$rootScope_, $templateCache) {
             $compile = _$compile_;
             $rootSscope = _$rootScope_;
             scope = $rootSscope.$new();

             $templateCache.put("app/views/header.directive.view.html","<div>Fake</div>");
        }));

        it("Should get current url", function () {
            var element = $compile("<xf-header></xf-header>")(scope);

            scope.$digest();

            var isolateScope = element.isolateScope();

            expect(isolateScope.current()).toEqual("testUrl");
        });

        it("Should set collapse on", function () {
            var element = $compile("<xf-header></xf-header>")(scope);

            scope.$digest();

            var isolateScope = element.isolateScope();

            isolateScope.collapse();

            expect(isolateScope.collapseIn).toBeTruthy();
        });
    });
});
