# Test Page #
## Installation ##
To start working with page you must first build it.

1. You must intall node.js
2. Next install modules: <code>npm install bower -g</code> <code>npm install gulp -g</code>
3. Go to main x-formation directory and install dependencies <code>npm install</code>
4. Install bower dependencies <code>bower install</code>
5. Now you can build project <code>gulp build</codE>
6. When build will be finished you can copy files from <code>dist</code> folder to your server.

You can also run prepared simple node server by command <code>node server.js</codes>.
Page will start from <code>dist</code> folder.

In this folder (<code>/dist</code>) use python simple http server by <code>sudo python -m http.server 8080</code> for Python3 or <code>sudo python -m SimpleHTTPServer 8080</code> for Python2.7.

## Development ##

To work in development envirment you just after installation run <code>gulp watch</code>

Source files are in <code>src</code> folder.

Tests files are in <code>tests</code> folder.
