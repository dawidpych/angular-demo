/*jshint ignore: start*/

module.exports = function (config) {
    config.set({
        basePath: ".",
        frameworks: ["jasmine", "requirejs"],
        files: [
            "tests/test-app.js",
            {
                pattern: "node_modules/angular-mocks/angular-mocks.js",
                included: false
            },
            {
                pattern: "src/**/*.js",
                included: false
            },
            {
                pattern : "tests/**/*.spec.js",
                included: false
            }
        ],
        reporters: ["progress", "coverage", "spec"],
        preprocessors: {
            "src/app/**/*.js": "coverage"
        },
        coverageReporter: {
            type: 'html',
            dir: 'coverage'
        },
        browsers: [
            "PhantomJS"
        ],
        plugins: [
            "karma-phantomjs-launcher",
            "karma-jasmine",
            "karma-requirejs",
            "karma-coverage",
            "karma-spec-reporter"
        ],
   });
};

/*jshint ignore: end*/
